package com.carbonit.ludovicpanel.katas.bankaccount.formatters;

import com.carbonit.ludovicpanel.katas.bankaccount.Operation;
import com.carbonit.ludovicpanel.katas.bankaccount.OperationType;
import com.carbonit.ludovicpanel.katas.bankaccount.StatementFormatter;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class TabularStatementFormatterTest {

    @Test
    void given_empty_operations_should_print_header() {
        StatementFormatter statementFormatter = new TabularStatementFormatter();

        List<Operation> operations = new ArrayList<>();

        String statementFormatted = statementFormatter.formatStatements(operations);
        assertThat(statementFormatted, is("DATE | OPERATION_TYPE | AMOUNT | BALANCE"));
    }

    @Test
    void given_one_deposit_should_print_statement() {
        StatementFormatter statementFormatter = new TabularStatementFormatter();

        Operation deposit = new Operation(new BigDecimal(1000), LocalDateTime.of(2018, 1, 1, 1, 30), new BigDecimal(1000), OperationType.DEPOSIT);

        List<Operation> operations = new ArrayList<>();
        operations.add(deposit);

        String statementFormatted = statementFormatter.formatStatements(operations);
        assertThat(statementFormatted, is(
                "DATE | OPERATION_TYPE | AMOUNT | BALANCE" +
                        "\n01/01/2018 01:30 | DEPOSIT | 1000.00 | 1000.00")
        );
    }

    @Test
    void given_one_deposit_and_withdraw_should_print_statement() {
        StatementFormatter statementFormatter = new TabularStatementFormatter();

        Operation deposit = new Operation(new BigDecimal(1000), LocalDateTime.of(2018, 1, 1, 1, 30), new BigDecimal(1000), OperationType.DEPOSIT);
        Operation withdraw = new Operation(new BigDecimal(500), LocalDateTime.of(2018, 1, 2, 1, 30), new BigDecimal(500), OperationType.WITHDRAW);

        List<Operation> operations = new ArrayList<>();
        operations.add(deposit);
        operations.add(withdraw);

        String statementFormatted = statementFormatter.formatStatements(operations);
        assertThat(statementFormatted, is(
                "DATE | OPERATION_TYPE | AMOUNT | BALANCE" +
                        "\n02/01/2018 01:30 | WITHDRAW | 500.00 | 500.00" +
                        "\n01/01/2018 01:30 | DEPOSIT | 1000.00 | 1000.00")
        );
    }
}