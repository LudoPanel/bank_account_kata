package com.carbonit.ludovicpanel.katas.bankaccount;

import com.carbonit.ludovicpanel.katas.bankaccount.exceptions.NegativeAmountException;
import com.carbonit.ludovicpanel.katas.bankaccount.exceptions.NotEnoughMoneyException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AccountTest {

    private static final BigDecimal INITIAL_BALANCE = new BigDecimal(100);

    @Spy
    private OperationRepository operationRepository;

    @Mock
    private DateTimeProvider dateTimeProvider;

    @Mock
    private StatementFormatter statementFormatter;

    @Spy
    private Printer printer;

    private Account account;

    @BeforeEach
    void setUp() {
        account = new Account(INITIAL_BALANCE, operationRepository, dateTimeProvider, printer, statementFormatter);
    }

    @Test
    void given_an_account_with_no_operations_should_have_a_balance_equals_to_0() {
        assertThat(account.getCurrentBalance(), is(INITIAL_BALANCE));
    }

    @Test
    void should_make_a_deposit_operation() {
        BigDecimal amount = new BigDecimal(1000);
        BigDecimal expectedBalance = INITIAL_BALANCE.add(amount);

        when(dateTimeProvider.getDateTime()).thenReturn(LocalDateTime.of(2018, 9, 18, 1, 30));

        Operation expectedOperation = new Operation(amount, dateTimeProvider.getDateTime(), expectedBalance, OperationType.DEPOSIT);

        account.makeDeposit(amount);

        ArgumentCaptor<Operation> argument = ArgumentCaptor.forClass(Operation.class);
        verify(operationRepository).add(argument.capture());
        assertEquals(expectedOperation, argument.getValue());
        assertEquals(expectedBalance, account.getCurrentBalance());
    }

    @Test
    void should_throw_exception_if_deposit_a_negative_amount() {
        BigDecimal amount = new BigDecimal(-1000);
        Assertions.assertThrows(NegativeAmountException.class, () -> account.makeDeposit(amount), "The amount have to be positive for a deposit");
    }

    @Test
    void should_make_a_withdraw_if_balance_is_sufficient() {
        BigDecimal amount = new BigDecimal(10);
        BigDecimal expectedBalance = INITIAL_BALANCE.add(amount.negate());

        when(dateTimeProvider.getDateTime()).thenReturn(LocalDateTime.of(2018, 9, 18, 1, 30));

        Operation expectedOperation = new Operation(amount, dateTimeProvider.getDateTime(), expectedBalance, OperationType.WITHDRAW);

        account.makeWithdraw(amount);

        ArgumentCaptor<Operation> argument = ArgumentCaptor.forClass(Operation.class);
        verify(operationRepository).add(argument.capture());
        assertEquals(expectedOperation, argument.getValue());
        assertEquals(expectedBalance, account.getCurrentBalance());
    }

    @Test
    void should_not_make_a_withdraw_if_not_enough_money() {
        BigDecimal amount = new BigDecimal(200);
        Assertions.assertThrows(NotEnoughMoneyException.class, () -> account.makeWithdraw(amount), "You don't have enough money to withdraw this amount : 200");
    }

    @Test
    void should_print_statement() {
        when(dateTimeProvider.getDateTime()).thenReturn(LocalDateTime.of(2018, 9, 18, 1, 30));

        List<Operation> operations = Arrays.asList(
                new Operation(new BigDecimal(10), dateTimeProvider.getDateTime(), new BigDecimal(110), OperationType.DEPOSIT),
                new Operation(new BigDecimal(10), dateTimeProvider.getDateTime(), new BigDecimal(100), OperationType.WITHDRAW)
        );

        String statementFormatted = "DATE | OPERATION_TYPE | AMOUNT | BALANCE" +
                "\n18/09/2018 01:30 | WITHDRAW | 10.00 | 100.00" +
                "\n18/09/2018 01:30 | DEPOSIT | 10.00 | 110.00";

        when(operationRepository.findAll()).thenReturn(operations);
        when(statementFormatter.formatStatements(operations)).thenReturn(statementFormatted);

        account.printStatement();

        ArgumentCaptor<String> argument = ArgumentCaptor.forClass(String.class);
        verify(printer).print(argument.capture());
        assertEquals(statementFormatted, argument.getValue());
    }

}