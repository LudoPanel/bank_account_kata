package com.carbonit.ludovicpanel.katas.bankaccount.integrationTests;

import com.carbonit.ludovicpanel.katas.bankaccount.*;
import com.carbonit.ludovicpanel.katas.bankaccount.formatters.TabularStatementFormatter;
import com.carbonit.ludovicpanel.katas.bankaccount.printers.ConsolePrinter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class BankAccountIntegrationTest {

    private static final BigDecimal INITIAL_BALANCE = BigDecimal.ZERO;

    private final OperationRepository operationRepository = new InMemoryOperationRepository();
    private final StatementFormatter statementFormatter = new TabularStatementFormatter();
    private final Printer printer = new ConsolePrinter();
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Mock
    private DateTimeProvider dateTimeProvider;

    private Account account;

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outContent));
        account = new Account(INITIAL_BALANCE, operationRepository, dateTimeProvider, printer, statementFormatter);
    }

    @Test
    void should_add_operations_and_print_statement() {
        given(dateTimeProvider.getDateTime()).willReturn(
                LocalDateTime.of(2018, 1, 1, 1, 30),
                LocalDateTime.of(2018, 1, 6, 1, 30),
                LocalDateTime.of(2018, 1, 15, 1, 30)
        );

        account.makeDeposit(new BigDecimal(1000));
        account.makeWithdraw(new BigDecimal(500));
        account.makeDeposit(new BigDecimal(1000));
        account.printStatement();

        assertThat(account.getCurrentBalance(), is(new BigDecimal(1500)));
        assertThat(operationRepository.findAll().size(), is(3));
        assertThat(outContent.toString().trim(), is(
                "DATE | OPERATION_TYPE | AMOUNT | BALANCE\n" +
                        "15/01/2018 01:30 | DEPOSIT | 1000.00 | 1500.00\n" +
                        "06/01/2018 01:30 | WITHDRAW | 500.00 | 500.00\n" +
                        "01/01/2018 01:30 | DEPOSIT | 1000.00 | 1000.00"
        ));
    }
}