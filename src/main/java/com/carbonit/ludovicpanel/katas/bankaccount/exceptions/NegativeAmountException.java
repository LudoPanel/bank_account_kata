package com.carbonit.ludovicpanel.katas.bankaccount.exceptions;

public class NegativeAmountException extends IllegalArgumentException {
    public NegativeAmountException(String message) {
        super(message);
    }
}
