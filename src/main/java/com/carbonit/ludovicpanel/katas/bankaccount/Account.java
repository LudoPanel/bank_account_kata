package com.carbonit.ludovicpanel.katas.bankaccount;

import com.carbonit.ludovicpanel.katas.bankaccount.exceptions.NegativeAmountException;
import com.carbonit.ludovicpanel.katas.bankaccount.exceptions.NotEnoughMoneyException;

import java.math.BigDecimal;

public class Account {

    private BigDecimal currentBalance;
    private final OperationRepository operationRepository;
    private final Printer printer;
    private final StatementFormatter statementFormatter;
    private final DateTimeProvider dateTimeProvider;

    public Account(BigDecimal initialBalance, OperationRepository operationRepository, DateTimeProvider dateTimeProvider, Printer printer, StatementFormatter statementFormatter) {
        this.currentBalance = initialBalance;
        this.operationRepository = operationRepository;
        this.printer = printer;
        this.statementFormatter = statementFormatter;
        this.dateTimeProvider = dateTimeProvider;
    }

    public void makeDeposit(BigDecimal amount) {
        boolean isAmountNegativeValue = amount.compareTo(BigDecimal.ZERO) < 0;
        if(isAmountNegativeValue) {
            throw new NegativeAmountException("The amount have to be positive for a deposit");
        }

        BigDecimal updatedBalance = currentBalance.add(amount);
        Operation deposit = new Operation(amount, dateTimeProvider.getDateTime(), updatedBalance, OperationType.DEPOSIT);
        operationRepository.add(deposit);
        currentBalance = updatedBalance;
    }

    public void makeWithdraw(BigDecimal amount) {
        boolean isBalanceGreaterThanAmountToBeWithdrawn = currentBalance.compareTo(amount) < 0;

        if(isBalanceGreaterThanAmountToBeWithdrawn) {
            throw new NotEnoughMoneyException("You don't have enough money to withdraw this amount : " + amount);
        }

        BigDecimal updatedBalance = currentBalance.add(amount.negate());
        Operation withdraw = new Operation(amount, dateTimeProvider.getDateTime(), updatedBalance, OperationType.WITHDRAW);
        operationRepository.add(withdraw);
        currentBalance = updatedBalance;
    }

    public void printStatement() {
        printer.print(statementFormatter.formatStatements(operationRepository.findAll()));
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }
}
