package com.carbonit.ludovicpanel.katas.bankaccount;

public enum OperationType {
    DEPOSIT,
    WITHDRAW
}
