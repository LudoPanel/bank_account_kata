package com.carbonit.ludovicpanel.katas.bankaccount;

public interface Printer {
    void print(String text);
}
