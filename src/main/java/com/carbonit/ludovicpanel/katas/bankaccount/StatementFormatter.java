package com.carbonit.ludovicpanel.katas.bankaccount;

import java.util.List;

public interface StatementFormatter {
    String formatStatements(List<Operation> operations);
}
