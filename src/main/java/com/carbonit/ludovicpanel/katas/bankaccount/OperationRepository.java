package com.carbonit.ludovicpanel.katas.bankaccount;

import java.util.List;

public interface OperationRepository {
    void add(Operation operation);
    List<Operation> findAll();
}
