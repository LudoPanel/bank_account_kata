package com.carbonit.ludovicpanel.katas.bankaccount;

import java.time.LocalDateTime;

public interface DateTimeProvider {
    LocalDateTime getDateTime();
}
