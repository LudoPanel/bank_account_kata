package com.carbonit.ludovicpanel.katas.bankaccount.printers;

import com.carbonit.ludovicpanel.katas.bankaccount.Printer;

public class ConsolePrinter implements Printer {
    public void print(String text) {
        System.out.println(text);
    }
}
