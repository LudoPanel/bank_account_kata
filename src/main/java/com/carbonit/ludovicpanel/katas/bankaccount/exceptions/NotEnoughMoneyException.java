package com.carbonit.ludovicpanel.katas.bankaccount.exceptions;

public class NotEnoughMoneyException extends IllegalArgumentException  {
    public NotEnoughMoneyException(String message) {
        super(message);
    }
}
