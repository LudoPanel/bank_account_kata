package com.carbonit.ludovicpanel.katas.bankaccount.formatters;

import com.carbonit.ludovicpanel.katas.bankaccount.Operation;
import com.carbonit.ludovicpanel.katas.bankaccount.StatementFormatter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;

public class TabularStatementFormatter implements StatementFormatter {

    private static final String HEADER = "DATE | OPERATION_TYPE | AMOUNT | BALANCE";
    private static final String FORMAT_DATE = "dd/MM/yyyy HH:mm";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT_DATE).withZone(ZoneOffset.UTC);

    public String formatStatements(List<Operation> operations) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(HEADER);
        operations.stream().sorted(Comparator.comparing(Operation::getInstant).reversed()).forEach(operation ->  this.formatOperationDetail(operation, stringBuilder));
        return stringBuilder.toString();
    }

    private void formatOperationDetail(Operation operation, StringBuilder stringBuilder) {
        stringBuilder.append("\n");
        stringBuilder.append(LocalDateTime.ofInstant(operation.getInstant(), ZoneOffset.UTC).format(formatter));
        stringBuilder.append(" | ");
        stringBuilder.append(operation.getOperationType());
        stringBuilder.append(" | ");
        stringBuilder.append(operation.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP));
        stringBuilder.append(" | ");
        stringBuilder.append(operation.getBalance().setScale(2, BigDecimal.ROUND_HALF_UP));
    }
}
