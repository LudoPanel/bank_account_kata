package com.carbonit.ludovicpanel.katas.bankaccount;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Objects;

public class Operation {

    private BigDecimal amount;
    private Instant instant;
    private BigDecimal balance;
    private OperationType operationType;

    public Operation(BigDecimal amount, LocalDateTime instant, BigDecimal balance, OperationType typeOperation) {
        this.amount = amount;
        this.instant = instant.toInstant(ZoneOffset.UTC);
        this.balance = balance;
        this.operationType = typeOperation;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Instant getInstant() {
        return instant;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Operation operation = (Operation) o;
        return Objects.equals(amount, operation.amount) &&
                Objects.equals(instant, operation.instant) &&
                Objects.equals(balance, operation.balance) &&
                operationType == operation.operationType;
    }
}
